﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebApplicationTest.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TestsController : ControllerBase
    {
        // GET api/tests
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return new string[] { "testValue1", "testValue2" };
        }

        // GET api/tests/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value: " + id;
        }
    }
}